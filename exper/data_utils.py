from functools import reduce, singledispatchmethod

import numpy as np
import pandas as pd
import torch

import pywt

import math


from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler


batch_size = 32

timestamp_col_old = " Timestamp"
timestamp_col = "Timestamp"
target_col = "Normal/Attack"

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


class TimeseriesDataset(torch.utils.data.Dataset):
    def __init__(
        self,
        dataframe: pd.DataFrame,
        window_size: int = 200,
        scaler: MinMaxScaler = None,
        subset: int = None,
        return_wavelet: bool = False,
    ) -> None:
        super().__init__()
        self.window_size = window_size

        self.scaler = scaler

        # read dataframe
        self.dataframe = dataframe
        self.dataframe = self.dataframe.rename(columns={col: col.strip(" ") for col in self.dataframe.columns})

        self.dataframe[target_col] = self.dataframe[target_col].apply(lambda x: 0 if "N" in x else 1)

        for col in self.dataframe.columns[1:]:
            self.dataframe[col] = self.dataframe[col].values.astype(np.float32)

        self.return_wavelet = return_wavelet

        self._data_process = False

        # if self.scaler is None:
        #     self.fit_scaler()

    @property
    def columns(self):
        return list(self.dataframe.columns)[1:-1]

    def column_info(self):
        column_info = []
        for idx, col in enumerate(self.dataframe.columns[1:-1]):
            info = {
                "idx": idx,
                "max": self.dataframe[col].max(),
                "min": self.dataframe[col].min(),
                "name": col,
            }
            column_info.append(info)
        return column_info

    def fit_scaler(self):
        self.scaler = MinMaxScaler(feature_range=(0, 1), clip=True)
        self.scaler = self.scaler.fit(self.dataframe.iloc[:, 1:-1])

    def _scale_data(self):
        print(f"scaling dataframe...")
        self.dataframe.iloc[:, 1:-1] = self.scaler.transform(self.dataframe.iloc[:, 1:-1])
        return self

    def data_process(self, scale_data: bool = False):

        if scale_data:
            self._scale_data()

        # convert to pytorch for easier training stuff
        self.x = torch.as_tensor(self.dataframe.iloc[:, 1:-1].values, dtype=torch.float32)
        self.y = torch.as_tensor(self.dataframe.iloc[:, -1].values, dtype=torch.float32).unsqueeze(1)

        if self.return_wavelet:
            self.generate_wavelet()

        self._data_process = True
        return self

    def generate_wavelet(self, wavelet_fn=pywt.Wavelet("db1")):
        values = self.dataframe.values[:, 1:-1]
        self.wavelet = []
        for idx in range(0, len(values) - self.window_size):
            cA, cD = pywt.dwt(
                values[idx : idx + self.window_size].reshape(values.shape[-1], self.window_size),
                wavelet=wavelet_fn,
                mode="periodic",
            )
            wavelet_row = np.concatenate([cA, cD], axis=1).reshape(self.window_size, values.shape[-1])
            self.wavelet.append(wavelet_row)
        self.wavelet = torch.as_tensor(self.wavelet, dtype=torch.float32)

    def get_labels(self):
        return self.dataframe.iloc[self.window_size :, -1].values

    def __len__(self):
        return len(self.x) - self.window_size

    @singledispatchmethod
    def __getitem__(self, key):
        raise NotImplementedError("not implemented for type")

    @__getitem__.register
    def _(self, key: int):

        x = self.x[key : key + self.window_size]
        y = self.y[key + self.window_size]

        if self.return_wavelet:
            x_wavelet = self.wavelet[key]
            return (x, x_wavelet), y

        return x, y

    @__getitem__.register
    def _(self, key: slice):
        x = torch.stack([self.x[idx : idx + self.window_size] for idx in range(key.start, key.stop)])
        y = torch.stack([self.y[idx + self.window_size] for idx in range(key.start, key.stop)])
        return x, y


def get_dataframes(small: bool, file_ext: str):
    normal_data_filepath = f"data/normal{'_subset' if small else ''}.{file_ext}"
    attack_data_filepath = f"data/attack{'_subset' if small else ''}.{file_ext}"

    pd_data_reader = {
        "xlsx": pd.read_excel,
        "csv": pd.read_csv,
    }
    print(f"reading file...")
    normal_dataframe = pd_data_reader[file_ext](normal_data_filepath)
    attack_dataframe = pd_data_reader[file_ext](attack_data_filepath)
    return normal_dataframe, attack_dataframe
