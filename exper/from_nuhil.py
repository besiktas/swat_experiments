class util_vars:

    window_size=200
    forecast_window=10
    kernel_sz = 4
    nb_filter = 5
    dilation_rates = [2**i for i in range(8)]
    nb_epoch = 20
    batch_size = 500
    dense_activations = ['sigmoid','relu','relu','sigmoid','sigmoid']
    loss_functions = ['categorical_crossentropy', 'mse', 'mse', 'categorical_crossentropy', 'categorical_crossentropy']
    model_metrics = ['accuracy', 'mse', 'mse', 'accuracy', 'accuracy']
    output_sizes = [3,1,1,2,2]


    # Init
    def __init__(self):
        self.mean = np.arange(5)
        self.std = np.arange(5)


    def get_mean(self):
        return self.mean


    def set_mean(self, mean):
        self.mean = mean


    def get_std(self):
        return self.std


    def set_std(self, std):
        return self.std


    # Make time-series instance
    def make_timeseries_instances(self, timeseries, window_size, forecast_window):
        timeseries = np.asarray(timeseries)
        assert 0 < window_size < timeseries.shape[0]
        X = np.atleast_3d(np.array([timeseries[start:start + window_size] for start in range(0, timeseries.shape[0] - window_size-forecast_window, 1)]))
        y = timeseries[window_size:] # Change this if output size is to change too
        y = np.array([y[start:start+forecast_window] for start in range(0,y.shape[0]-forecast_window, 1)])
        q = np.atleast_3d([timeseries[-window_size:]])
        return X, y, q



import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from IPython.display import Image
# %matplotlib inline
import math
# Import utils
# import util_vars

# All vars|
util = util_vars()

# from tensorflow import tensorflow.keras

from tensorflow.keras.models import Model
from tensorflow.keras.layers import Input, Dense, LSTM, Conv1D, MaxPooling1D, Flatten, Reshape, Dropout, concatenate, GlobalMaxPooling1D
# from tensorflow.keras.layers.wrappers import TimeDistributed
from tensorflow.keras.utils import plot_model


import pywt
wavelet = pywt.Wavelet('db1')


# Read Data
dataset = pd.read_excel('data/normal.xlsx')
full_attack = pd.read_excel('data/attack.xlsx')
full_attack = full_attack.drop(full_attack.index[230310:264238]).reset_index(drop=True)
full_attack.columns


dataset['Normal/Attack'] = dataset['Normal/Attack'].apply(lambda x: 0 if 'N' in x else 1)
full_attack['Normal/Attack'] = full_attack['Normal/Attack'].apply(lambda x: 0 if 'N' in x else 1)
normal_data = dataset.values[16000:, 1:].astype('float16') #Only need data after first 16,000
attack_data = full_attack.values[:, 1:].astype('float16')
full_attack = None
dataset = None


 # Get labels
y_labels_attack = attack_data[:, -1].astype('int')
normal_data = normal_data[:, :-1]
attack_data = attack_data[:, :-1]
normal_data.shape, attack_data.shape


from sklearn.preprocessing import MinMaxScaler

scaler = MinMaxScaler(feature_range=(0,1))
test_size = int(normal_data.shape[0]*0.5)
scaler = scaler.fit(normal_data[:-test_size,:])
attack_scaled = scaler.transform(attack_data)
normal_data = None
attack_data = None
attack_scaled.shape

# In [23]: attack_scaled.shape
# Out[23]: (415991, 51)

util.window_size = 200
util.forecast_window = 1
util.nb_filter = 5
util.nb_epoch = 100


# Time Window Build of Data
normal_scaled = None
X = np.atleast_3d(np.array([attack_scaled[start:start + util.window_size] for start in range(0, attack_scaled.shape[0] - util.window_size-util.forecast_window, 1)]))
y = y_labels_attack[util.window_size+1:]
y.shape, X.shape

# In [25]: y.shape, X.shape
# Out[25]: ((415790,), (415790, 200, 51))


from tqdm import tqdm as tqdm

arr = np.zeros((415790,200,102))
for i, elm in enumerate(tqdm(X)):
    cA, cD = pywt.dwt(X[i].reshape(51,200), wavelet=wavelet, mode='periodic')
    arr[i] = np.concatenate((X[i],np.concatenate((cA, cD), axis=1).reshape(200,51)), axis=1)

np.savez('data.npz', X=X, y=y)


# Train - Test split for time window
X = arr
train_size = int(X.shape[0]*0.6)
X_train, X_test = X[:-train_size], X[-train_size:]
y_train, y_test = y[:-train_size], y[-train_size:]
full_X_train, full_X_test = attack_scaled[util.window_size+1:-train_size], attack_scaled[-train_size:]
X_test.shape, X_train.shape, y_test.shape, y_train.shape, full_X_test.shape, full_X_train.shape


full_X_train_t = full_X_train.reshape(full_X_train.shape[0], 1, full_X_train.shape[1])
full_X_valid_t = full_X_train.reshape(full_X_train.shape[0], 1, full_X_train.shape[1])


util.nb_epoch = 30


def multi_input(input_, full):
    x = Input(shape=(util.window_size, X_train.shape[2]), name='window_input')

    cnn_out = Dense(100)(x)
    cnn_out = Conv1D(filters=util.nb_filter, kernel_size=4, activation='relu',
               padding='causal', dilation_rate=2)(cnn_out)
    cnn_out = MaxPooling1D()(cnn_out)
    cnn_out = Conv1D(filters=util.nb_filter*2, kernel_size=4, activation='relu',
               padding='causal', dilation_rate=4)(cnn_out)
    cnn_out = MaxPooling1D()(cnn_out)
    cnn_out = Conv1D(filters=util.nb_filter*3, kernel_size=4, activation='relu',
               padding='causal', dilation_rate=8)(cnn_out)
    cnn_out = MaxPooling1D()(cnn_out)
    cnn_out = Conv1D(filters=util.nb_filter*4, kernel_size=4, activation='relu',
               padding='causal', dilation_rate=16)(cnn_out)
    cnn_out = GlobalMaxPooling1D()(cnn_out)
#     cnn_out = Conv1D(filters=util.nb_filter*5, kernel_size=4, activation='relu',
#                padding='causal', dilation_rate=16)(cnn_out)
#     cnn_out = GlobalMaxPooling1D()(cnn_out)

    # Non time-window input
    input_full = Input(shape=(1,full.shape[1]), name='full_input')
    lstm_out = LSTM(128, return_sequences=True)(input_full)
    lstm_out = LSTM(128)(lstm_out)
#     lstm_out = LSTM(util.nb_filter*4)(lstm_out)

    # Concat LSTM out and y
    y = concatenate([lstm_out, cnn_out])
#     y = Reshape((int(util.nb_filter*4/normal_data.shape[1])*2,normal_data.shape[1]))(y)
    y = Dropout(0.2)(y)
#     y = TimeDistributed(Dense(normal_data.shape[1], activation='sigmoid', name='main_output'))(y)
    y = Dense(64, activation='sigmoid')(y)
    output = Dense(1, activation='sigmoid', name='main_output')(y)

    model = Model(inputs=[x, input_full], outputs=output, name='cnn_lstm_model')
    return model


multi_model = multi_input(input_=X_train, full=full_X_train)


# In [36]: for layer in multi_model.layers:
#     ...:     print(f"{layer.name} input_shape: {layer.input_shape} | output_shape: {layer.output_shape}")
#     ...:
# window_input input_shape: [(None, 200, 102)] | output_shape: [(None, 200, 102)]
# dense input_shape: (None, 200, 102) | output_shape: (None, 200, 100)
# conv1d input_shape: (None, 200, 100) | output_shape: (None, 200, 5)
# max_pooling1d input_shape: (None, 200, 5) | output_shape: (None, 100, 5)
# conv1d_1 input_shape: (None, 100, 5) | output_shape: (None, 100, 10)
# max_pooling1d_1 input_shape: (None, 100, 10) | output_shape: (None, 50, 10)
# conv1d_2 input_shape: (None, 50, 10) | output_shape: (None, 50, 15)
# full_input input_shape: [(None, 1, 51)] | output_shape: [(None, 1, 51)]
# max_pooling1d_2 input_shape: (None, 50, 15) | output_shape: (None, 25, 15)
# lstm input_shape: (None, 1, 51) | output_shape: (None, 1, 128)
# conv1d_3 input_shape: (None, 25, 15) | output_shape: (None, 25, 20)
# lstm_1 input_shape: (None, 1, 128) | output_shape: (None, 128)
# global_max_pooling1d input_shape: (None, 25, 20) | output_shape: (None, 20)
# concatenate input_shape: [(None, 128), (None, 20)] | output_shape: (None, 148)
# dropout input_shape: (None, 148) | output_shape: (None, 148)
# dense_1 input_shape: (None, 148) | output_shape: (None, 64)
# main_output input_shape: (None, 64) | output_shape: (None, 1)

# Compile Model
multi_model.compile(loss=['binary_crossentropy'], optimizer='adam', metrics=['accuracy'])

from collections import Counter
def get_class_weights(y, smooth_factor=0):
    counter = Counter(y)
    if smooth_factor > 0:
        p = max(counter.values()) * smooth_factor
        for k in counter.keys():
            counter[k] += p
    majority = max(counter.values())
    return {cls: float(majority / count) for cls, count in counter.items()}


class_weights = get_class_weights(y_train, smooth_factor = 0)
class_weights

# Save model to disk
def save_model_to_disk(model_, name="model"):
    model_.save(name+'.h5')
    print("----- {0} SAVED TO DISK ----".format(name))


# In [44]: full_X_train_t.shape
# Out[44]: (166316, 1, 51)

# In [45]: X_train.shape
# Out[45]: (166316, 200, 102)

multi_model.fit({'window_input':X_train, 'full_input':full_X_train_t}, y_train, epochs=util.nb_epoch,
                class_weight=class_weights, batch_size=util.batch_size)

save_model_to_disk(multi_model, name="wavenetCNNLSTM_1")


import gc

del X
del arr
del X_train
del full_X_train_t

gc.collect()
gc.collect()

results = multi_model.evaluate([X_test, full_X_test.reshape(full_X_test.shape[0], 1, full_X_test.shape[1])], y_test, batch_size=32)

results = multi_model.predict([X_test, full_X_test.reshape(full_X_test.shape[0], 1, full_X_test.shape[1])], batch_size=32)
print(f"results on test set: {results}")
# predictions = multi_model.predict([X_test, full_X_test.reshape(full_X_test.shape[0], 1, full_X_test.shape[1])])

# predictions[predictions>=0.5] = 1
# predictions[predictions<0.5] = 0

# y_test.shape

# from sklearn.metrics import recall_score
# recall_score(predictions.reshape(predictions.shape[0],), y_test)

# np.unique(predictions)

# # array([0., 1.], dtype=float32)