import numpy as np
import pywt
import torch
import torch.nn as nn
import torch.nn.functional as F

wavelet = pywt.Wavelet("db1")


class LSTM_Section(nn.Module):
    def __init__(self, in_channels: int):
        self.in_channels = in_channels
        self.hidden_sz = 128
        self.dropout_amt = 0.2
        self.lstm_1 = nn.LSTM(self.in_channels, self.hidden_sz, dropout=self.dropout_amt, num_layers=2, batch_first=True)

    def forward(self, input):
        output, (_, _) = self.lstm_1(input)
        return output[:, -1]


class FeedforwardModel(nn.Module):
    def __init__(self):
        super(FeedforwardModel, self).__init__()

        self.linear_1 = nn.LazyLinear(100)
        self.activation_1 = nn.Sigmoid()
        self.linear_2 = nn.LazyLinear(100)
        self.activation_2 = nn.Sigmoid()
        self.output = nn.LazyLinear(1)

    def forward(self, x):

        x = torch.flatten(x, 1)
        x = self.linear_1(x)
        x = self.activation_1(x)
        x = self.linear_2(x)
        x = self.activation_2(x)
        return self.output(x)

    def _reset_module_params(self):
        self.linear_1.reset_parameters()
        self.linear_2.reset_parameters()
        self.output.reset_parameters()


class Model(nn.Module):
    def __init__(self, add_wavelet: bool = False, include_lstm: bool = False):
        super(Model, self).__init__()

        self.include_lstm = include_lstm
        self.add_wavelet = add_wavelet

        dilation = 2
        nb_filter = 32
        kernel_sz = 4
        dropout_amt = 0.2
        hidden_sz = 128

        # CNN section
        self.linear_1 = nn.LazyLinear(100)

        self.cnn_1 = nn.LazyConv1d(nb_filter, kernel_size=kernel_sz, dilation=dilation)
        self.relu_1 = nn.ReLU()

        self.cnn_2 = nn.LazyConv1d(nb_filter * 2, kernel_size=kernel_sz, dilation=dilation ** 2)
        self.relu_2 = nn.ReLU()

        self.cnn_3 = nn.LazyConv1d(nb_filter * 3, kernel_size=kernel_sz, dilation=dilation ** 3)
        self.relu_3 = nn.ReLU()

        self.cnn_4 = nn.LazyConv1d(nb_filter * 4, kernel_size=kernel_sz)
        self.relu_4 = nn.ReLU()

        # LSTM section
        if include_lstm:
            print(f"using lstm...")
            self.lstm_1 = nn.LSTM(51, hidden_sz, num_layers=2, dropout=dropout_amt, batch_first=True)

        # last section
        self.dropout = nn.Dropout(dropout_amt)
        self.linear_end = nn.LazyLinear(128)
        self.sigmoid_act = nn.Sigmoid()
        self.output_layer = nn.LazyLinear(1)

    def _reset_module_params(self):
        self.linear_1.reset_parameters()
        self.cnn_1.reset_parameters()
        self.cnn_2.reset_parameters()
        self.cnn_3.reset_parameters()
        self.cnn_4.reset_parameters()

    def forward(self, x, x_wavelet=None):

        x_cnn = x

        # compute the wavelet in forward so we can use gradient methods that only take x tensor input
        if self.add_wavelet:
            x_wavelet = pywt.dwt(x.detach().cpu().numpy().reshape(1, x.shape[2], x.shape[1]), wavelet=wavelet, mode="periodic")
            x_wavelet = torch.from_numpy(np.concatenate([*x_wavelet], axis=1)).reshape(*x.shape)
            x_cnn = torch.cat([x, x_wavelet], dim=2)

        x_cnn = self.linear_1(x_cnn)

        x_cnn = F.max_pool1d(self.relu_1(self.cnn_1(x_cnn)), 1)
        x_cnn = F.max_pool1d(self.relu_2(self.cnn_2(x_cnn)), 1)
        x_cnn = F.max_pool1d(self.relu_3(self.cnn_3(x_cnn)), 1)
        x_cnn = F.adaptive_max_pool1d(self.relu_4(self.cnn_4(x_cnn)), 1)
        x_cnn = torch.flatten(x_cnn, 1)

        # very unclear whether to use first val or hn or what?
        if self.include_lstm:
            x_lstm, (hn, _) = self.lstm_1(x)
            x_out = torch.cat([x_lstm[:, -1], x_cnn], dim=1)
        else:
            x_out = x_cnn

        x_out = self.dropout(x_out)
        x_out = self.sigmoid_act(self.linear_end(x_out))

        # dont need sigmoid since using BCEWithLogitsLoss
        return self.output_layer(x_out)
