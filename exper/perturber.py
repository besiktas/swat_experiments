import torch

from typing import Any, Dict

from exper.tracking import CaptureManager


class EasyDict:
    def __init__(self, **kwargs):
        for key, val in kwargs.items():
            self.__setattr__(key, val)


class Perturber:
    """this is the type for perturbing a sample"""

    def __init__(
        self,
        mean: float = 0.0,
        std: float = 0.2,
        distribution: torch.distributions.Distribution = torch.distributions.normal.Normal,
    ) -> None:
        self.mean = mean
        self.std = std

        self.dist = distribution(self.mean, self.std, validate_args=False)

    def __call__(self, x: torch.Tensor):
        return self.dist.sample(x.size())


class PerturbCapture:
    """this is the type that captures

    Returns:
        [type]: [description]
    """

    def __init__(
        self,
        model: torch.nn.Module = None,
        capture_manager: CaptureManager = None,
        perturb: Perturber = Perturber(std=0.1),
        n_perturbations: int = 100,
    ):
        self.device = next(model.parameters()).device

        # self.capture = capture
        # self.capture_manager = CaptureManager(model)
        if model:
            self.capture_manager = CaptureManager(model)
        elif capture_manager:
            self.capture_manager = capture_manager
        self.perturb = perturb
        self.n_perturbations = n_perturbations

    def __call__(self, x: torch.Tensor) -> Dict[str, Any]:
        x_perturbed = x.repeat(self.n_perturbations, 1, 1, 1).to(self.device)
        x_perturbed += self.perturb(x_perturbed).to(self.device)

        return self.capture_manager(x_perturbed, None)


class LayerStats:
    def __init__(self, layer_name):
        self.__name__ = layer_name
        self._set_kwds = []

        self.svd_vals = []
        self.used_gather = False

        self.normal_ratio = []
        self.log_ratio = []
        self.explained_variance = []
        self.explained_variance_log = []

    def __call__(self, *args: Any, **kwds: Any) -> Any:
        # pass
        for kwd, val in kwds.items():
            if kwd not in self.__dict__:
                self._set_kwds.append(kwd)
                self.__dict__[kwd] = []
            self.__dict__[kwd].append(val)

    def add_activations(self, activations: torch.Tensor):
        self.svd_vals.append(torch.linalg.svdvals(activations.reshape(activations.size(0), -1)))

    def gather(self):
        self.used_gather = True

        self.svd_vals = torch.stack(self.svd_vals)
        self.svd_vals_log = torch.log(self.svd_vals)

        self.normal_ratio.append(torch.mean(self.svd_vals[:, 0] / self.svd_vals[:, 1]))
        self.log_ratio.append(torch.mean(self.svd_vals[:, 0] / self.svd_vals[:, 1]))

        self.explained_variance.append(torch.mean(torch.Tensor([svd[0] ** 2 / sum(svd ** 2) for svd in self.svd_vals])))
        self.explained_variance_log.append(torch.mean(torch.Tensor([svd[0] ** 2 / sum(svd ** 2) for svd in self.svd_vals_log])))

        self.svd_vals = []


class PerturbStats:
    """usage:
    svdstats = PerturbStats()
    svdstats.relu1.explained_variance(torch.mean(val))

    """

    def __init__(self, *layers) -> None:
        self._layers = {}
        for layer in layers:
            self._layers[layer] = LayerStats(layer)
            setattr(self, layer, self._layers[layer])

    def __call__(self, activations: Dict[str, torch.Tensor]):
        for layer in self._layers.keys():
            self._layers[layer].add_activations(activations[layer])

    def gather(self):
        for _, layerstats in self._layers.items():

            svd_vals = torch.stack(layerstats.svd_vals)
            svd_vals_log = torch.log(svd_vals)

            svd_vals_avg = torch.mean()

            normal_ratio = torch.mean(svd_vals[:, 0] / svd_vals[:, 1])
            log_ratio = torch.mean(svd_vals[:, 0] / svd_vals[:, 1])

            explained_variance = torch.mean(torch.Tensor([svd[0] ** 2 / sum(svd ** 2) for svd in svd_vals]))
            explained_variance_log = torch.mean(torch.Tensor([svd[0] ** 2 / sum(svd ** 2) for svd in svd_vals_log]))

            layerstats(
                normal_ratio=normal_ratio,
                log_ratio=log_ratio,
                svd_vals_avg=svd_vals_avg,
                explained_variance=explained_variance,
                explained_variance_log=explained_variance_log,
            )

            layerstats.svd_vals = []
            # obj.gather()
