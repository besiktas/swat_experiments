from pathlib import Path

import torch
import torch.nn as nn

from typing import List


class CaptureActivations:
    def __init__(self, module):
        self.output = None
        self.handle = module.register_forward_hook(self)

    def __call__(self, module: nn.Module, input: torch.Tensor, output: torch.Tensor):
        self.output = output

    def __del__(self):
        self.handle.remove()


class CaptureManager:
    def __init__(self, model: nn.Module, layers: List[str] = []):
        self.model = model

        self.layers_to_attach = {}
        for layer_name, layer_obj in list(self.model.named_children()):
            if (len(layers) > 0) and (layer_name in layers):
                self.layers_to_attach[layer_name] = layer_obj
            else:
                self.layers_to_attach[layer_name] = layer_obj
            # if isinstance(layer_obj, nn.Conv1d):
            #     continue

        self.capture = {}
        self.output = {}

        self.data = []
        self.preds = []
        self.labels = []

    def __call__(self, x: torch.Tensor, y: torch.Tensor = None, set_attach_detach: bool = True, clear_after: bool = True):

        if set_attach_detach:
            self.attach()

        self.preds = self.model(x)
        self.batch(x, y)

        activations = self.output

        if set_attach_detach:
            self.detach()

        if clear_after:
            self.clear()

        return activations

    def clear(self):
        self.output = {}
        self.data = []
        self.labels = []

    def attach(self):
        self.capture = {key: CaptureActivations(layer) for key, layer in self.layers_to_attach.items()}

    def detach(self):
        self.capture = {key: None for key in self.layers_to_attach.keys()}

    def batch_multiple(self, x: torch.Tensor, y: torch.Tensor):
        self.data.extend(x)
        self.labels.extend(y)

        for key in self.capture.keys():
            if key not in self.output:
                self.output[key] = []

        for output in self.capture[key].output.detach().cpu():
            self.output[key].append(output)

    def batch(self, x: torch.Tensor, y: torch.Tensor = None):
        # just handle single for now
        self.data = x
        self.labels = y

        for key in self.capture.keys():
            self.output[key] = self.capture[key].output.detach().cpu()

    def save_output(self, path: Path):
        # not sure if i want to save as numpy or torch, just saving for the time being to work on server
        info = {}
        info_path = path / "info.pt"

        info["input"] = self.data
        info["labels"] = self.labels
        info["preds"] = self.preds
        info["activations"] = {}

        for idx, layer_key in enumerate(self.output.keys()):
            activations = torch.tensor([mat.numpy() for mat in self.output[layer_key]])
            print(f"layer: {layer_key} shape: {activations.shape}")
            info["activations"][idx] = {"layer_name": layer_key, "activations": activations}
            # output_path = path / f"{key}.pt"
            # torch.save(output, output_path)
            # info[idx] = output_path

        # save the info related to
        torch.save(info, info_path)

    def get_activations(self, x, y, clear_after: bool = True):
        self(x, y)
        activations = self.output
        if clear_after:
            self.clear()
        return activations
