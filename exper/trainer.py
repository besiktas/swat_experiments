from typing import Callable

import torch
import torch.nn as nn

from exper import tracking
from exper.data_utils import device


class Trainer:
    def __init__(
        self,
        model: nn.Module,
        trainloader: torch.utils.data.DataLoader,
        testloader: torch.utils.data.DataLoader,
        loss_func: Callable[..., torch.Tensor],
        optimizer: torch.optim.Optimizer,
    ) -> None:
        self.model = model
        self.trainloader = trainloader
        self.testloader = testloader

        self.loss_func = loss_func
        self.optimizer = optimizer

        self.capture_manager = tracking.CaptureManager(self.model)

        self.perturb_helper = None
        self.svd_vals = None

    def train(self, n_epochs, use_testloader: bool = False):

        for epoch in range(n_epochs):
            print(f"==>epoch==>{epoch}")
            running_corrects = 0
            running_loss = 0.0

            self.model.train()
            with torch.enable_grad():
                for i, (x, y) in enumerate(self.trainloader):
                    x, y = x.to(device), y.to(device)
                    # with torch.set_grad_enabled(True):
                    self.optimizer.zero_grad()
                    preds = self.model(x)

                    loss = self.loss_func(preds, y)
                    loss.backward()
                    self.optimizer.step()

                    running_loss += loss.item()
                    running_corrects += torch.sum(preds.round() == y)

            acc = running_corrects / len(self.trainloader.dataset)
            phase = "train"
            print(
                f"{phase.upper().ljust(6)} || loss: {running_loss:.3f} | running correct: {running_corrects} | accuracy: {acc:.3f}"
            )

            self.test_step()

    def test_step(self):
        running_corrects = 0
        running_loss = 0.0
        self.model.eval()
        with torch.no_grad():
            for i, (x, y) in enumerate(self.testloader):
                x, y = x.to(device), y.to(device)
                preds = self.model(x)
                loss = self.loss_func(preds, y)
                running_loss += loss.item()

                running_corrects += torch.sum(preds.round() == y)

                if self.perturb_helper:
                    svdvals = self.perturb_and_get_svd(x[0, :, :].unsqueeze(0))
                    expvar = (svdvals[0] ** 2) / sum(svdvals ** 2)
                    print(f"got expvar: {expvar} and first svdval:{svdvals[0]}")

        acc = running_corrects / len(self.testloader.dataset)
        phase = "test"
        print(f"{phase.upper().ljust(6)} || loss: {running_loss:.3f} | running correct: {running_corrects} | accuracy: {acc:.3f}")

    def perturb_and_get_svd(self, x, n_perturbations=100):
        # just handle single x for now

        x_perturb = x.repeat(n_perturbations, 1, 1).to(device)

        for col_idx in range(x.shape[-1]):
            x_perturb[:, :, col_idx] += self.perturb_helper.perturb_col(col_idx, x_perturb.shape[:-1]).to(device)

        activations = self.capture_manager(x_perturb, None)

        keys_ = ["relu_1", "relu_2"]
        # for key in keys_:

        svdvals = torch.linalg.svdvals(activations["relu_1"].reshape(n_perturbations, -1))
        return svdvals

    def post_train_step(self):
        for func in self.post_train_callbacks:
            func()
