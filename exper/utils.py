import argparse
from os import PathLike
import torch


def get_args():
    parser = argparse.ArgumentParser()

    # args for train.py
    parser.add_argument("--small", default=False, action="store_true")
    parser.add_argument("--n_epochs", default=5, type=int)
    parser.add_argument("--save_model", action="store_true")
    parser.add_argument("--file_ext", default="csv")
    parser.add_argument("--scale_data", default=True, action="store_true")
    parser.add_argument("--lstm", default=False, action="store_true")

    # args for perturb.py
    parser.add_argument("--model_type", default="latest")
    parser.add_argument("--perturb_col", default="AIT201")

    return parser.parse_args()


def save_model(model: torch.nn.Module, filepath: str) -> None:
    torch.save(model.state_dict(), filepath)


def load_model(filepath: str) -> torch.nn.Module:
    return torch.load(filepath)
