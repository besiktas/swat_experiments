# https://github.com/cleverhans-lab/cleverhans/blob/master/tutorials/torch/mnist_tutorial.py

from absl import app, flags

# from easydict import EasyDict
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision

from typing import Any, Dict, List

# from datasets import MNISTDataset

import matplotlib.pyplot as plt

from cleverhans.torch.attacks.fast_gradient_method import fast_gradient_method
from cleverhans.torch.attacks.projected_gradient_descent import (
    projected_gradient_descent,
)

from exper.tracking import CaptureManager
from exper.perturber import EasyDict, Perturber, PerturbStats, PerturbCapture

FLAGS = flags.FLAGS

DEVICE = "cuda" if torch.cuda.is_available() else "cpu"
mnist_datainfo = EasyDict(in_channels=1, dataset=torchvision.datasets.MNIST, root="/data/graham/data/mnist")

# import mlflow
# import mlflow.pytorch

import wandb

# wandb.config.update(flags.FLAGS)
wandb.init(project="svd_experiments")


class CNN(torch.nn.Module):
    """Basic CNN architecture."""

    def __init__(self, in_channels=1):
        super(CNN, self).__init__()
        self.conv1 = nn.Conv2d(in_channels, 64, 8, 1)  # (batch_size, 3, 28, 28) --> (batch_size, 64, 21, 21)
        self.conv2 = nn.Conv2d(64, 128, 6, 2)  # (batch_size, 64, 21, 21) --> (batch_size, 128, 8, 8)
        self.conv3 = nn.Conv2d(128, 128, 5, 1)  # (batch_size, 128, 8, 8) --> (batch_size, 128, 4, 4)
        self.fc1 = nn.Linear(128 * 4 * 4, 128)  # (batch_size, 128, 4, 4) --> (batch_size, 2048)
        self.fc2 = nn.Linear(128, 10)  # (batch_size, 128) --> (batch_size, 10)

        self.relu1 = nn.ReLU()
        self.relu2 = nn.ReLU()
        self.relu3 = nn.ReLU()

    def forward(self, x):
        # x = F.relu(self.conv1(x))
        # x = F.relu(self.conv2(x))
        # x = F.relu(self.conv3(x))

        x = self.conv1(x)
        x = self.relu1(x)
        x = self.conv2(x)

        x = self.relu2(x)
        x = self.conv3(x)
        x = self.relu3(x)

        x = x.view(-1, 128 * 4 * 4)
        x = self.fc1(x)
        x = self.fc2(x)
        return x


class PyNet(nn.Module):
    """CNN architecture. This is the same MNIST model from pytorch/examples/mnist repository"""

    def __init__(self, in_channels=1):
        super(PyNet, self).__init__()
        self.conv1 = nn.Conv2d(in_channels, 32, 3, 1)
        self.conv2 = nn.Conv2d(32, 64, 3, 1)
        self.dropout1 = nn.Dropout(0.25)
        self.dropout2 = nn.Dropout(0.5)
        self.fc1 = nn.Linear(9216, 128)
        self.fc2 = nn.Linear(128, 10)

        self.relu1 = nn.ReLU()
        self.relu2 = nn.ReLU()
        self.relu3 = nn.ReLU()

    def forward(self, x):
        x = self.conv1(x)
        # x = F.relu(x)
        x = self.relu1(x)
        x = self.conv2(x)
        # x = F.relu(x)
        x = self.relu2(x)
        x = F.max_pool2d(x, 2)
        x = self.dropout1(x)
        x = torch.flatten(x, 1)
        x = self.fc1(x)
        # x = F.relu(x)
        x = self.relu3(x)
        x = self.dropout2(x)
        x = self.fc2(x)
        output = F.log_softmax(x, dim=1)
        return output


def ld_mnist():
    """Load training and test data."""
    train_transforms = torchvision.transforms.Compose([torchvision.transforms.ToTensor()])
    test_transforms = torchvision.transforms.Compose([torchvision.transforms.ToTensor()])

    # Load MNIST dataset
    train_dataset = torchvision.datasets.MNIST(root=mnist_datainfo.root, transform=train_transforms)
    test_dataset = torchvision.datasets.MNIST(root=mnist_datainfo.root, train=False, transform=test_transforms)

    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=64, shuffle=True, num_workers=2)
    test_loader = torch.utils.data.DataLoader(test_dataset, batch_size=64, shuffle=False, num_workers=2)
    return EasyDict(train=train_loader, test=test_loader)


def main(_):
    # Load training and test data
    data = ld_mnist()

    # mlflow.set_tracking_uri("http://localhost:5000")
    # mlflow.pytorch.autolog()
    # mlflow.set_tags({"perturb_std": FLAGS.perturb_std, "perturb_type": FLAGS.perturb_type})

    wandb.config.update(flags.FLAGS)

    # Instantiate model, loss, and optimizer for training
    if FLAGS.model == "cnn":
        net = CNN(in_channels=1)

    elif FLAGS.model == "pynet":
        net = PyNet(in_channels=1)
    else:
        raise NotImplementedError

    device = DEVICE
    if device == "cuda":
        net = net.cuda()
    loss_fn = torch.nn.CrossEntropyLoss(reduction="mean")
    optimizer = torch.optim.Adam(net.parameters(), lr=1e-3)

    n_perturbations = 30
    layers_ = ["relu1", "relu2", "relu3"]
    perturber = Perturber(std=FLAGS.perturb_std)
    capture_manager = CaptureManager(net, layers=layers_)
    perturb_capture = PerturbCapture(net, perturber)
    pstats = PerturbStats(*layers_)

    loss_plot = []

    log_ratios = {val: [] for val in range(1, 4)}
    normal_ratios = {val: [] for val in range(1, 4)}
    explained_variance = {val: [] for val in range(1, 4)}
    explained_variance_log = {val: [] for val in range(1, 4)}

    # Train vanilla model
    net.train()

    # print(f"epoch: 1 || 0.541 | 0.644 | 0.623 || 0.010 | 0.028 | 0.040 || 3.558 |  3.158 | 2.741 || 1.241 | 1.215 | 1.191")
    print(f"         || expv1 | expv2 | expv3 || elog1 | elog2 | elog3 || rat1  |  rat2  | rat3  || lrat1 | lrat2 | lrat3")
    for epoch in range(1, FLAGS.nb_epochs + 1):
        train_loss = 0.0

        for data_idx, (x, y) in enumerate(data.train):
            x, y = x.to(device), y.to(device)
            if FLAGS.adv_train:
                # Replace clean example with adversarial example for adversarial training
                x = projected_gradient_descent(net, x, FLAGS.eps, 0.01, 40, np.inf)
            optimizer.zero_grad()
            loss = loss_fn(net(x), y)
            loss.backward()
            optimizer.step()
            train_loss += loss.item()

            if data_idx < 20:
                x = x[0] if FLAGS.perturb_type == "single" else x
                activations = perturb_capture(x)
                pstats(activations)

        print("epoch: {}/{}, train loss: {:.3f}".format(epoch, FLAGS.nb_epochs, train_loss))
        loss_plot.append(train_loss)
        pstats.gather()

        print(
            f"epoch: {epoch} | "
            + f"{pstats.relu1.explained_variance[-1]:.3f} | {pstats.relu2.explained_variance[-1]:.3f} | {pstats.relu3.explained_variance[-1]:.3f} || "
            + f"{pstats.relu1.explained_variance_log[-1]:.3f} | {pstats.relu2.explained_variance_log[-1]:.3f} | {pstats.relu3.explained_variance_log[-1]:.3f} || "
            + f"{pstats.relu1.normal_ratio[-1]:.3f} |  {pstats.relu2.normal_ratio[-1]:.3f} | {pstats.relu3.normal_ratio[-1]:.3f} || "
            + f"{pstats.relu1.log_ratio[-1]:.3f} | {pstats.relu2.log_ratio[-1]:.3f} | {pstats.relu3.log_ratio[-1]:.3f}"
        )

    fig, axs = plt.subplots(5, 1, figsize=(14, 24))

    axs[0].plot(loss_plot)
    axs[0].set_title(f"loss")

    for idx, stat in enumerate(["explained_variance", "explained_variance_log", "normal_ratio"]):
        for layer in ["relu1", "relu2", "relu3"]:
            layer_stats = getattr(pstats, layer)
            axs[idx + 1].plot(getattr(layer_stats, stat), label=layer)

    for layer in ["relu1", "relu2", "relu3"]:
        layer_obj = getattr(pstats, layer)
        axs[4].plot(layer_obj.log_ratio, label=layer)

    axs[1].set_title("explained_variance")
    axs[2].set_title("explained_variance_log")
    axs[3].set_title("normal_ratios")
    axs[4].set_title("log_ratios")

    axs[1].legend()
    axs[2].legend()
    axs[3].legend()
    axs[4].legend()

    # single means x[0] in perturbing while multiple will be x
    fig.savefig(f"plots/image_based/mnist_ratio_{FLAGS.perturb_type}_std{FLAGS.perturb_std}.png", dpi=600)
    # mlflow.log_artifact(f"plots/image_based/mnist_ratio_{FLAGS.perturb_type}_std{FLAGS.perturb_std}.png")
    wandb.log(
        {
            f"mnist_ratio_{FLAGS.perturb_std}": wandb.Image(
                f"plots/image_based/mnist_ratio_{FLAGS.perturb_type}_std{FLAGS.perturb_std}.png"
            )
        }
    )

    # Evaluate on clean and adversarial data
    net.eval()
    report = EasyDict(nb_test=0, correct=0, correct_fgm=0, correct_pgd=0)
    for x, y in data.test:
        x, y = x.to(device), y.to(device)
        x_fgm = fast_gradient_method(net, x, FLAGS.eps, np.inf)
        x_pgd = projected_gradient_descent(net, x, FLAGS.eps, 0.01, 40, np.inf)
        _, y_pred = net(x).max(1)  # model prediction on clean examples
        _, y_pred_fgm = net(x_fgm).max(1)  # model prediction on FGM adversarial examples
        _, y_pred_pgd = net(x_pgd).max(1)  # model prediction on PGD adversarial examples
        report.nb_test += y.size(0)
        report.correct += y_pred.eq(y).sum().item()
        report.correct_fgm += y_pred_fgm.eq(y).sum().item()
        report.correct_pgd += y_pred_pgd.eq(y).sum().item()
    print("test acc on clean examples (%): {:.3f}".format(report.correct / report.nb_test * 100.0))
    print("test acc on FGM adversarial examples (%): {:.3f}".format(report.correct_fgm / report.nb_test * 100.0))
    print("test acc on PGD adversarial examples (%): {:.3f}".format(report.correct_pgd / report.nb_test * 100.0))


if __name__ == "__main__":
    flags.DEFINE_integer("nb_epochs", 8, "Number of epochs.")
    flags.DEFINE_float("eps", 0.3, "Total epsilon for FGM and PGD attacks.")
    flags.DEFINE_bool("adv_train", False, "Use adversarial training (on PGD adversarial examples).")
    flags.DEFINE_enum("model", "cnn", ["cnn", "pynet"], "Choose model type.")
    flags.DEFINE_enum("perturb_type", "single", ["single", "batch"], "Choose model type.")
    flags.DEFINE_float("perturb_std", 0.2, "STD for noise added")

    app.run(main)
