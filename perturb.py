from dataclasses import dataclass
import argparse

import numpy as np
from cleverhans.torch.attacks import fast_gradient_method

import matplotlib.pyplot as plt
import torch.multiprocessing as mp
import pandas as pd


from art.attacks.evasion import FastGradientMethod
from art.estimators.classification import PyTorchClassifier


from train import *
from exper.tracking import CaptureManager

from exper.utils import get_args


args = get_args()

model_type = args.model_type
model = torch.load(f"/data/graham/swat_experiments/data/model/{model_type}")
model.share_memory()
trainer.model = model

outputs_dir = Path("/data/graham/swat_experiments/data/outputs") / model_type

outputs_dir.mkdir(parents=True, exist_ok=True)

x, _ = attack_train_dataset[0]


classifier = PyTorchClassifier(
    model=model,
    clip_values=(0.0, 1.0),
    loss=loss_func,
    optimizer=optimizer,
    input_shape=(1, x.shape[-2], x.shape[-1]),
    nb_classes=1,
)

breakpoint()


@dataclass
class AttackInfo:
    start_idx: int
    end_idx: int = None


def generate_attack_info(dataset: TimeseriesDataset = attack_test_dataset):
    attacks = []
    curr_attack = None
    for n in range(len(dataset)):
        _, y = dataset[n]
        if curr_attack:
            if y.item() != 1:
                curr_attack.end_idx = n
                attacks.append(curr_attack)
                curr_attack = None
        else:
            if y.item() == 1:
                curr_attack = AttackInfo(n)

    return attacks


class PerturbTracker:
    def __init__(self, col, col_idx, col_mean, col_std):
        self.col = col
        self.col_idx = col_idx

        self.col_mean = col_mean
        self.col_std = col_std

        self.capture_manager = CaptureManager(model)

        self._perturb_distribution = torch.distributions.uniform.Uniform

        self._activations_all = {}

    def get_activations(self, x, y):
        if len(x.shape) == 2:
            x = x.unsqueeze(0)
        return self.capture_manager(x.to(device), y.to(device))

    def new_sample(self, x, y):
        # get baseline for x,y input that we will perturb
        self.x = x
        self.y = y

        self.x_activations = self.get_activations(x, y)
        self.collected_activations = {key: [val.flatten()] for key, val in self.x_activations.items()}
        self.saved_norms = {key: [] for key in self.x_activations.keys()}

        self.perturb_dist = self._perturb_distribution(-sigma * self.col_std, sigma * self.col_std)

    def perturb(self, sigma, n_perturbations):
        self.sigma = sigma

        self.perturb_dist = self._perturb_distribution(-sigma * self.col_std, sigma * self.col_std)

        # x_perturb = self.x.detach().clone()
        x_perturb = self.x.repeat(n_perturbations, 1, 1).to(device)
        perturb_sample = self.perturb_dist.sample(x_perturb.shape[:-1]).to(device)

        x_perturb[:, :, self.col_idx] += perturb_sample

        new_activations = self.get_activations(x_perturb, self.y.repeat(n_perturbations, 1))

        for key in new_activations.keys():

            self.saved_norms[key].append(torch.linalg.matrix_norm(self.x_activations[key] - new_activations[key]))
            self.collected_activations[key].extend(new_activations[key].reshape(n_perturbations, -1))

    def _perturb(self, sigma, n_perturbations):
        self.perturb_dist = self._perturb_distribution(-sigma * self.col_std, sigma * self.col_std)
        for n in range(n_perturbations):

            # p = mp.Process(target=train, args=(model,))
            x_perturb = self.x.detach().clone().cpu()
            x_perturb[:, self.col_idx] += self.perturb_dist.sample((x_perturb.shape[0],))

            new_activations = self.get_activations(x_perturb, self.y)

            for key, val in new_activations.items():
                self.saved_norms[key].append(torch.linalg.matrix_norm(self.x_activations[key] - new_activations[key]))
                self.collected_activations[key].append(val.flatten())

    def perturbed_statistics(self, svd_first_n=5):
        self.svd_vals = {}
        self.avg_norms = {}

        for key, val in self.collected_activations.items():

            self.svd_vals[key] = torch.linalg.svdvals(torch.stack(val)).tolist()

            if svd_first_n:
                self.svd_vals[key] = self.svd_vals[key][:svd_first_n]

            self.avg_norms[key] = torch.mean(torch.stack(self.saved_norms[key])).tolist()
        return self.svd_vals, self.avg_norms

    def collect_activations(self):
        for key, val in self.collected_activations.items():
            if key not in self._activations_all:
                self._activations_all[key] = []
            self._activations_all[key].extend(val)

    def calculate_svd_all(self):
        svd_vals = {}
        for key, val in self._activations_all.items():
            svd_vals[key] = torch.linalg.svdvals(torch.stack(val)).tolist()

        self._activations_all = {}
        return svd_vals


attacks = generate_attack_info()

attacks = attacks[0:5]
print(f"got n attacks: {len(attacks)}")
dataset = attack_test_dataset
baseline_dataset = attack_train_dataset

sigma_vals = [0.01, 1.0, 2.0]
n_perturbs = 100

eps = 0.3
norm = np.inf
clip_min = 0
clip_max = 1


class Status:
    BEFORE = 0
    DURING = 1
    AFTER = 2


stats = {sigma: {} for sigma in sigma_vals}


class StatTracker:
    def __init__(self):
        # self.status = status
        self.col = {}

    def __call__(self, col, attack_idx, sigma, idx, info):
        _col = self.col.get(col, {})
        _attack_idx = _col.get(attack_idx, {})
        _sigma = _attack_idx.get(sigma, {})
        _sigma[idx] = info

        self.col[col] = _col


n_before_after = 50

before_attack_tracker = StatTracker()
adv_before_attack_tracker = StatTracker()
during_attack_tracker = StatTracker()
adv_during_attack_tracker = StatTracker()


# def

for col_idx, col in enumerate(dataset.columns):
    df = []

    df_svd = []
    print(f"doing col: {col}")
    col_mean = baseline_dataset.x[:, col_idx].mean()
    col_std = baseline_dataset.x[:, col_idx].std()

    if (col_mean or col_std) == 0.0:
        print(f"dont do this column: {col}")
        continue

    before_attack_perturbs = PerturbTracker(col, col_idx, col_mean, col_std)
    during_attack_perturbs = PerturbTracker(col, col_idx, col_mean, col_std)

    before_attack_perturbs_adv = PerturbTracker(col, col_idx, col_mean, col_std)
    during_attack_perturbs_adv = PerturbTracker(col, col_idx, col_mean, col_std)

    for attack_idx, attack_obj in enumerate(attacks):
        print(f"attack_idx:[{attack_idx}]")
        before_attack_idxs = list(range(attack_obj.start_idx - n_before_after, attack_obj.start_idx))
        during_attack_idxs = list(range(attack_obj.start_idx, attack_obj.start_idx + n_before_after))

        for sigma in sigma_vals:
            for idx in before_attack_idxs:
                print(f"before_attack_idxs:[{idx}]")
                x, y = dataset[idx]

                before_attack_perturbs.new_sample(x, y)
                before_attack_perturbs.perturb(sigma, n_perturbs)
                svd_vals, avg_norms = before_attack_perturbs.perturbed_statistics()
                before_attack_perturbs.collect_activations()

                df.append([attack_idx, col, "before", idx, "normal", sigma, svd_vals, avg_norms])

                adv_x = fast_gradient_method(model, x.unsqueeze(0).to(device), eps, norm, clip_min, clip_max)

                before_attack_perturbs_adv.new_sample(adv_x, y)
                before_attack_perturbs_adv.perturb(sigma, n_perturbs)
                svd_vals, avg_norms = before_attack_perturbs_adv.perturbed_statistics()
                before_attack_perturbs_adv.collect_activations()

                df.append([attack_idx, col, "before", idx, "adversarial", sigma, svd_vals, avg_norms])

            svd_vals = before_attack_perturbs.calculate_svd_all()
            svd_vals_adv = before_attack_perturbs_adv.calculate_svd_all()

            df_svd.append([attack_idx, col, "before", "normal", sigma, svd_vals])
            df_svd.append([attack_idx, col, "before", "adversarial", sigma, svd_vals_adv])

            for idx in during_attack_idxs:
                print(f"during_attack_idxs:[{idx}]")
                x, y = dataset[idx]

                during_attack_perturbs.new_sample(x, y)
                during_attack_perturbs.perturb(sigma, n_perturbs)
                svd_vals, avg_norms = during_attack_perturbs.perturbed_statistics()
                during_attack_perturbs.collect_activations()

                df.append([attack_idx, col, "during", idx, "normal", sigma, svd_vals, avg_norms])

                adv_x = fast_gradient_method(model, x.unsqueeze(0).to(device), eps, norm, clip_min, clip_max)

                during_attack_perturbs_adv.new_sample(adv_x, y)
                during_attack_perturbs_adv.perturb(sigma, n_perturbs)
                svd_vals, avg_norms = during_attack_perturbs_adv.perturbed_statistics()
                during_attack_perturbs_adv.collect_activations()

                df.append([attack_idx, col, "during", idx, "adversarial", sigma, svd_vals, avg_norms])

            svd_vals = during_attack_perturbs.calculate_svd_all()
            svd_vals_adv = during_attack_perturbs_adv.calculate_svd_all()

            df_svd.append([attack_idx, col, "during", "normal", sigma, svd_vals])
            df_svd.append([attack_idx, col, "during", "adversarial", sigma, svd_vals_adv])

    df_svd = pd.DataFrame(df_svd, columns=["attack_idx", "dataset_column", "time_status", "attack_type", "sigma_value", "svd"])

    df = pd.DataFrame(
        df, columns=["attack_idx", "dataset_column", "time_status", "data_idx", "attack_type", "sigma_value", "svd", "norm"]
    )

    df.to_csv(outputs_dir / f"{col}_dataframe.csv")
    df_svd.to_csv(outputs_dir / f"{col}_svd_dataframe.csv")
