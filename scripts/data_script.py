import pandas as pd

subset_size = 10000

normal_dataframe = pd.read_excel("./data/normal.xlsx")
attack_dataframe = pd.read_excel("./data/attack.xlsx")


# normal_subset_test = pd.read_excel("../data/normal_subset.xlsx")
# attack_subset_test = pd.read_excel("../data/attack_subset.xlsx")


def make_subset_excel(subset_size: int = 10000):

    normal_dataframe_size = normal_dataframe.size
    attack_dataframe_size = attack_dataframe.size

    print(f"normal size: {normal_dataframe_size}, attack size: {attack_dataframe_size}")

    normal_subset = normal_dataframe.iloc[:subset_size, :]
    attack_subset = attack_dataframe.iloc[:subset_size, :]

    for df, filename in [(normal_subset, "normal_subset.xlsx"), (attack_subset, "attack_subset.xlsx")]:
        with pd.ExcelWriter(f"./data/{filename}") as writer:
            df.to_excel(writer, index=False)


def convert_to_csv():
    normal_dataframe.to_csv("./data/normal.csv", index_label=False)
    attack_dataframe.to_csv("./data/attack.csv", index_label=False)


if __name__ == "__main__":
    # make_subset_excel()
    convert_to_csv()
