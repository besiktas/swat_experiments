from exper.tracking import CaptureManager
import os.path
from pathlib import Path

import numpy as np

from fastapi import FastAPI, Request, Response, File, UploadFile
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from pydantic import BaseModel
from typing import List

import torch

from webapp.server_helper import *

app = FastAPI()


app.mount("/static", StaticFiles(directory="webapp/static"), name="static")
app.add_middleware(CORSMiddleware, allow_origins=["*"], allow_credentials=True, allow_methods=["*"], allow_headers=["*"])
templates = Jinja2Templates(directory="webapp/templates")

attack_test_dataset.data_process(scale_data=True)
capture_manager = CaptureManager(model=model)


def _get_activations(sample_id: int, new_x: torch.Tensor = None):
    x, y = attack_test_dataset[sample_id]

    if new_x is not None:
        x = x.clone().detach()
        # print(f" difference between vals: {x[-1, :] - new_x}")
        x[-1, :] = new_x

    x = x.unsqueeze(0)
    x, y = x.to(device), y.to(device)
    activations = capture_manager(x, y)

    obj = {
        "sample_id": sample_id,
        "columns": attack_test_dataset.column_info(),
        "x": x.tolist(),
        "y": y.tolist(),
        "preds": capture_manager.preds.tolist(),
        "activations": {key: val.tolist() for key, val in activations.items()},
    }
    return obj


@app.get("/", response_class=HTMLResponse)
async def main(request: Request):
    result = "Hello from main"
    return templates.TemplateResponse("main.html", {"request": request, "result": result})


@app.get("/sample")
def get_activations():
    rand_idx = torch.randint(0, len(attack_test_dataset), (1,)).item()
    return _get_activations(rand_idx)


@app.get("/sample/{sample_id}")
def get_activations_for_sample(sample_id: int):
    return _get_activations(sample_id)


@app.post("/sample/{sample_id}")
async def post_activations_for_sample(sample_id: int, request: Request):
    data = await request.json()
    new_x_vals = torch.Tensor([float(val) for val in data])

    new_obj = _get_activations(sample_id, new_x_vals)
    old_obj = _get_activations(sample_id)

    differences = {
        key: (torch.Tensor(old_obj["activations"][key]) - torch.Tensor(new_obj["activations"][key])).tolist()
        for key in new_obj["activations"].keys()
    }

    differences_norm = {
        key: torch.linalg.matrix_norm(
            (torch.Tensor(old_obj["activations"][key]) - torch.Tensor(new_obj["activations"][key]))
        ).tolist()
        for key in new_obj["activations"].keys()
    }
    new_obj["differences"] = differences
    new_obj["differences_norm"] = differences_norm

    return new_obj
