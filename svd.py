import torch
from scipy.stats import entropy

mat_i, mat_j = 100, 20

baseline_sample = torch.rand(mat_i, mat_j)

n_perturbations = 1000

for val in [1e3, 1e2, 1e1, 1, 1e-1, 1e-2, 1e-3]:
    min_val, max_val = -1 * val, 1 * val
    dist = torch.distributions.Uniform(min_val, max_val)

    normed_vals = []
    svd_vals = []
    entropy_vals = []

    svd_vals.append(baseline_sample.flatten().numpy())

    for idx in range(n_perturbations):
        perturbed_sample = baseline_sample + dist.sample((mat_i, mat_j))

        entropy_vals.append(entropy(perturbed_sample.flatten()))
        normed_vals.append(torch.linalg.matrix_norm(baseline_sample - perturbed_sample))
        svd_vals.append(perturbed_sample.flatten().numpy())

    svd_vals = torch.Tensor(svd_vals)
    s = torch.linalg.svdvals(svd_vals)

    s_sq = torch.square(s)
    print(
        f"for val: {str(val).ljust(6)} got normed mean {torch.mean(torch.Tensor(normed_vals)):.05f}, svd: {s[0:3]}, || svd var {torch.sum(s_sq[0:3])/torch.sum(s_sq)}"
    )

