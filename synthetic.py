import math
from functools import singledispatchmethod
from typing import List

import numpy as np
import torch.nn as nn
import torch

from sklearn.neural_network import MLPClassifier, MLPRegressor
from sklearn.metrics import mean_squared_error

from config import *
from exper.model import FeedforwardModel, Model
from exper.trainer import Trainer


class SyntheticDataset(torch.utils.data.Dataset):
    def __init__(self, timesteps: List[int], num_fixed: int, num_flexible: int, window_size: int = 200):
        self.num_signal = num_fixed
        self.num_noise = num_flexible
        self.window_size = window_size

        self._primes = [
            # 2,
            3,
            5,
            7,
            11,
            13,
        ]

        self.timesteps = timesteps

        # while self._primes

        x_vals_signal = []
        # x_vals_noise = []
        y = []
        for val in self.timesteps:
            # not sure how i want this
            x_vals_signal.append([math.sin(val * prime) for prime in self._primes[: self.num_signal]])
            x_vals_signal[-1].append(math.tan(val * 2))

            # should i include the noise in y?
            # x_vals_noise.append(torch.rand(self.num_noise))
            # also not sure if i want to use regression or classification?
            y_to_use = abs(math.prod(x_vals_signal[-1]))

            # y_to_use = 1 if (y_to_use > 60) else 0
            y.append(y_to_use)

        self.x = torch.Tensor(x_vals_signal)
        x_vals_noise = torch.rand((self.x.shape[0], self.num_noise))
        self.x = torch.cat([self.x, x_vals_noise], dim=1)

        self.y = torch.Tensor(y)

    @staticmethod
    def _get_primes(n):
        pass

    def data_process(self, *args, **kwargs):
        pass

        # for i in range(self.num_fixed):

    @singledispatchmethod
    def __getitem__(self, key):
        raise NotImplementedError("not implemented for type")

    @__getitem__.register
    def _(self, key: int):
        x = self.x[key : key + self.window_size]
        y = self.y[key + self.window_size]
        return x, y.unsqueeze(0)

    def __len__(self):
        return len(self.x) - self.window_size


# if __name__ == "__main__":
print("testing data_utils.py")

timesteps = list(range(0, 1000000))
split_idx = round(len(timesteps) * 0.7)
train_timesteps = timesteps[:split_idx]
test_timesteps = timesteps[split_idx:]
train_dataset = SyntheticDataset(train_timesteps, 2, 2)
test_dataset = SyntheticDataset(test_timesteps, 2, 2)


batch_size = 32

trainloader = torch.utils.data.DataLoader(train_dataset, batch_size=batch_size, shuffle=True, num_workers=2)
testloader = torch.utils.data.DataLoader(test_dataset, batch_size=batch_size, shuffle=True, num_workers=2)


device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

model = Model(add_wavelet=False, include_lstm=False).to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=0.005)
loss_func = nn.BCEWithLogitsLoss()
trainer = Trainer(model, trainloader, testloader, loss_func, optimizer)

# trainer.train(n_epochs=5)


# mlp = MLPClassifier(
#     hidden_layer_sizes=(50,), max_iter=10, alpha=1e-4, solver="sgd", verbose=10, random_state=1, learning_rate_init=0.1
# )

mlp = MLPRegressor(random_state=1, max_iter=100, solver="sgd", verbose=10, learning_rate_init=0.1)


x_train = train_dataset.x.numpy()
y_train = train_dataset.y.numpy()

x_test = test_dataset.x.numpy()
y_test = test_dataset.y.numpy()

# mlp.fit(x_train, y_train)


# print("Training set score: %f" % mlp.score(x_train, y_train))
# print("Test set score: %f" % mlp.score(x_test, y_test))


class SineApproximator(nn.Module):
    def __init__(self):
        super(SineApproximator, self).__init__()
        self.regressor = nn.Sequential(
            nn.LazyLinear(1024), nn.ReLU(inplace=True), nn.LazyLinear(1024), nn.ReLU(inplace=True), nn.Flatten(), nn.LazyLinear(1)
        )

    def forward(self, x):
        output = self.regressor(x)
        return output


LR = 1e-6
MAX_EPOCH = 10
BATCH_SIZE = 512

model = SineApproximator().to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=LR)
criterion = nn.MSELoss(reduction="mean")

# training loop
train_loss_list = list()
val_loss_list = list()

my_images = []


for epoch in range(MAX_EPOCH):
    print("epoch %d / %d" % (epoch + 1, MAX_EPOCH))
    model.train()
    ############
    # TRAINING loop
    temp_loss_list = list()
    for X_train, y_train in trainloader:
        X_train = X_train.type(torch.float32).to(device)
        y_train = y_train.type(torch.float32).to(device)
        optimizer.zero_grad()
        score = model(X_train)
        loss = criterion(input=score, target=y_train)
        loss.backward()
        optimizer.step()
        temp_loss_list.append(loss.detach().cpu().numpy())

    temp_loss_list = list()
    for X_train, y_train in testloader:
        X_train = X_train.type(torch.float32).to(device)
        y_train = y_train.type(torch.float32).to(device)
        score = model(X_train)
        loss = criterion(input=score, target=y_train)
        temp_loss_list.append(loss.detach().cpu().numpy())

    train_loss_list.append(np.average(temp_loss_list))

    # validation
    model.eval()

    temp_loss_list = list()
    for X_val, y_val in testloader:
        X_val = X_val.type(torch.float32).to(device)
        y_val = y_val.type(torch.float32).to(device)

        score = model(X_val)
        loss = criterion(input=score, target=y_val)

        temp_loss_list.append(loss.detach().cpu().numpy())

    val_loss_list.append(np.average(temp_loss_list))

    print("\ttrain loss: %.5f" % train_loss_list[-1])
    print("\tval loss: %.5f" % val_loss_list[-1])
