from dataclasses import dataclass
from pathlib import Path
from typing import Callable, Dict, List

import pandas as pd

import numpy as np


import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

import argparse

# from attack import fast_gradient_method
from cleverhans.torch.attacks.fast_gradient_method import fast_gradient_method

from exper.model import FeedforwardModel, Model
from exper import tracking
from config import *
from exper.data_utils import *
from exper.utils import get_args

from exper.trainer import Trainer

# device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
torch.multiprocessing.set_sharing_strategy("file_system")

print(f"using device: {device}")


##### #### #### ######
##### #### #### ######
#### SCRIPT BELOW ####
##### #### #### ######
##### #### #### ######


args = get_args()

normal_dataframe, attack_dataframe = get_dataframes(args.small, args.file_ext)
print(f"done reading data...")

normal_dataframe[timestamp_col_old] = pd.to_datetime(normal_dataframe[timestamp_col_old])
attack_dataframe[timestamp_col_old] = pd.to_datetime(attack_dataframe[timestamp_col_old])
attack_dataframe = attack_dataframe.drop(attack_dataframe.index[230310:264238]).reset_index(drop=True)

train_dataframe, test_dataframe = train_test_split(attack_dataframe, test_size=0.4, shuffle=False)

normal_dataset = TimeseriesDataset(normal_dataframe)
normal_dataset.fit_scaler()

attack_train_dataset = TimeseriesDataset(train_dataframe, scaler=normal_dataset.scaler, return_wavelet=False)
attack_test_dataset = TimeseriesDataset(test_dataframe, scaler=normal_dataset.scaler, return_wavelet=False)

print(f"processing data...")
attack_train_dataset.data_process(scale_data=args.scale_data)
attack_test_dataset.data_process(scale_data=args.scale_data)
perturber = PerturbHelper(attack_train_dataset.x)

trainloader = torch.utils.data.DataLoader(
    attack_train_dataset,
    batch_size=batch_size,
    shuffle=True,
    num_workers=2,
)

testloader = torch.utils.data.DataLoader(attack_test_dataset, batch_size=batch_size, shuffle=True, num_workers=2)

y_counts = attack_train_dataset.y.unique(return_counts=True)
y_counts_test = attack_test_dataset.y.unique(return_counts=True)

print(
    f"y_counts[0][1]|| train: {y_counts[1]} ({(y_counts[1][1]/y_counts[1].sum()):.3f}% attack )| test: {y_counts_test[1]} ({(y_counts_test[1][1]/y_counts_test[1].sum()):.3f}% attack)"
)


loss_func = nn.BCEWithLogitsLoss(pos_weight=y_counts[1][0] / y_counts[1][1])
model = Model(add_wavelet=False, include_lstm=args.lstm).to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=0.005)
trainer = Trainer(model, trainloader, testloader, loss_func, optimizer)


ff_loss_func = nn.BCEWithLogitsLoss(pos_weight=y_counts[1][0] / y_counts[1][1])
ff_model = FeedforwardModel().to(device)
ff_optimizer = torch.optim.Adam(ff_model.parameters(), lr=0.005)
ff_trainer = Trainer(ff_model, trainloader, testloader, ff_loss_func, ff_optimizer)


reset_params_model = Model(add_wavelet=False, include_lstm=args.lstm).to(device)
reset_params_optimizer = torch.optim.Adam(reset_params_model.parameters(), lr=0.005)
reset_params_trainer = Trainer(reset_params_model, trainloader, testloader, loss_func, optimizer)

trainer.perturb_helper = perturber
ff_trainer.perturb_helper = perturber
reset_params_trainer.perturb_helper = perturber


for i, (x, y) in enumerate(testloader):
    print("got x, y")
    break

if __name__ == "__main__":
    print(f"using small dataset: {args.small}")
    print(f"n epochs: {args.n_epochs}")

    print(f"=== ===\ndoing good model\n=== ===")
    trainer.train(n_epochs=args.n_epochs)
    if args.save_model:
        torch.save(trainer.model, "/data/graham/swat_experiments/data/model/latest")

    print(f"=== ===\ndoing feedforward model\n=== ===")
    ff_trainer.train(n_epochs=1)
    if args.save_model:
        ff_trainer.model._reset_module_params()
        torch.save(ff_trainer.model, "/data/graham/swat_experiments/data/model/ff_model")

    print(f"=== ===\ndoing reset_params_model\n=== ===")
    reset_params_trainer.train(n_epochs=1)
    if args.save_model:
        reset_params_trainer.model._reset_module_params()
        torch.save(reset_params_trainer.model, "/data/graham/swat_experiments/data/model/reset_params_model")
