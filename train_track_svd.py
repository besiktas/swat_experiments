from dataclasses import dataclass
from pathlib import Path
from typing import Callable, Dict, List

import pandas as pd

import numpy as np


import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

import argparse

# from attack import fast_gradient_method
from cleverhans.torch.attacks.fast_gradient_method import fast_gradient_method

from exper.model import Model
from exper import tracking
from config import *
from exper.data_utils import *
from exper.utils import get_args

from exper.trainer import Trainer
from exper.tracking import CaptureManager

# device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
torch.multiprocessing.set_sharing_strategy("file_system")

print(f"using device: {device}")


##### #### #### ######
##### #### #### ######
#### SCRIPT BELOW ####
##### #### #### ######
##### #### #### ######


@dataclass
class ColInfo:
    _min: torch.Tensor
    _max: torch.Tensor
    mean: torch.Tensor
    std: torch.Tensor

    skip: bool = False

    def __post_init__(self):
        if self._min == self._max:
            self.skip = True

        self.dist = torch.distributions.normal.Normal(self.mean, self.std, validate_args=False)


class PerturbColumnHelper:
    def __init__(self, x):
        self.trainer = None

        self.cols = {}
        for col_idx in range(x.shape[1]):
            col = x[:, col_idx]
            col_info = ColInfo(col.min(), col.max(), col.mean(), col.std())
            self.cols[col_idx] = col_info

    def perturb_col(self, col_idx: int, size):
        return self.cols[col_idx].dist.sample(size)


args = get_args()

normal_dataframe, attack_dataframe = get_dataframes(args.small, args.file_ext)
print(f"done reading data...")

normal_dataframe[timestamp_col_old] = pd.to_datetime(normal_dataframe[timestamp_col_old])
attack_dataframe[timestamp_col_old] = pd.to_datetime(attack_dataframe[timestamp_col_old])
attack_dataframe = attack_dataframe.drop(attack_dataframe.index[230310:264238]).reset_index(drop=True)

train_dataframe, test_dataframe = train_test_split(attack_dataframe, test_size=0.4, shuffle=False)

normal_dataset = TimeseriesDataset(normal_dataframe)
normal_dataset.fit_scaler()

attack_train_dataset = TimeseriesDataset(train_dataframe, scaler=normal_dataset.scaler, return_wavelet=False)
attack_test_dataset = TimeseriesDataset(test_dataframe, scaler=normal_dataset.scaler, return_wavelet=False)

print(f"processing data...")
attack_train_dataset.data_process(scale_data=args.scale_data)
attack_test_dataset.data_process(scale_data=args.scale_data)


trainloader = torch.utils.data.DataLoader(
    attack_train_dataset,
    batch_size=batch_size,
    shuffle=True,
    num_workers=2,
)

testloader = torch.utils.data.DataLoader(attack_test_dataset, batch_size=batch_size, shuffle=True, num_workers=2)

y_counts = attack_train_dataset.y.unique(return_counts=True)
y_counts_test = attack_test_dataset.y.unique(return_counts=True)

print(
    f"y_counts[0][1]|| train: {y_counts[1]} ({(y_counts[1][1]/y_counts[1].sum()):.3f}% attack )| test: {y_counts_test[1]} ({(y_counts_test[1][1]/y_counts_test[1].sum()):.3f}% attack)"
)


loss_func = nn.BCEWithLogitsLoss(pos_weight=y_counts[1][0] / y_counts[1][1])
model = Model(add_wavelet=False, include_lstm=args.lstm).to(device)
optimizer = torch.optim.Adam(model.parameters(), lr=0.005)
trainer = Trainer(model, trainloader, testloader, loss_func, optimizer)


perturber = PerturbHelper(attack_train_dataset.x)
trainer.perturb_helper = perturber
trainer.capture_manager = CaptureManager(model)

if __name__ == "__main__":
    print(f"using small dataset: {args.small}")
    print(f"n epochs: {args.n_epochs}")

    print(f"=== ===\ndoing good model\n=== ===")
    trainer.train(n_epochs=args.n_epochs)
    if args.save_model:
        torch.save(trainer.model, "/data/graham/swat_experiments/data/model/latest")
