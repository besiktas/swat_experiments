from exper.data_utils import *

normal_dataframe, attack_dataframe = get_dataframes(False, "csv")
print(f"done reading data...")

# normal_dataframe[timestamp_col_old] = pd.to_datetime(normal_dataframe[timestamp_col_old])
# attack_dataframe[timestamp_col_old] = pd.to_datetime(attack_dataframe[timestamp_col_old])
attack_dataframe = attack_dataframe.drop(attack_dataframe.index[230310:264238]).reset_index(drop=True)

print(f"splitting dataframe")
train_dataframe, test_dataframe = train_test_split(attack_dataframe, test_size=0.4, shuffle=False)

print("converting to timeseries dataset")
normal_dataset = TimeseriesDataset(normal_dataframe)
normal_dataset.fit_scaler()

attack_train_dataset = TimeseriesDataset(train_dataframe, scaler=normal_dataset.scaler, return_wavelet=False).data_process()
attack_test_dataset = TimeseriesDataset(test_dataframe, scaler=normal_dataset.scaler, return_wavelet=False)

# print(f"processing data...")
# attack_train_dataset.data_process()
# attack_test_dataset.data_process()

print("loading model")
model = torch.load("/data/graham/swat_experiments/data/model/latest")
