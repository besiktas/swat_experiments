document.addEventListener("DOMContentLoaded", (event) => {
  const inputSliders = document.querySelector("#dataInputs")
  inputSliders.addEventListener("change", async (event) => {
    const sampleId = document.getElementById("sampleId")

    const els = document.querySelectorAll(".rangSlider")

    let vals = [...els].map((val) => parseFloat(val.value))

    await changeSample(sampleId.innerText, vals)
  })
})

function removeAllForClass(cls) {
  var els = document.getElementsByClassName(cls)
  while (els[0]) {
    els[0].parentNode.removeChild(els[0])
  }
}

async function changeSample(sample_id, vals) {
  const response = await fetch(`/sample/${sample_id}`, {
    method: "POST",
    body: JSON.stringify(vals),
  })

  const data = await response.json()

  makeRangeSliders(data["sample_id"], data["x"][0], data["columns"])
  makeLabel(data["y"])

  var differences

  if (data["differences"] !== undefined) {
    addDifferences(data["differences"], data["differences_norm"])
  }
}

async function fetchData(method = "GET") {
  const response = await fetch("/sample")
  return await response.json()
}

function makeRangeSliders(sample_id, arr, column_info) {
  arr = arr[arr.length - 1]
  var el = document.getElementById("dataInputs")

  var sampleLabel = document.getElementById("sampleId")
  sampleLabel.innerText = sample_id

  for (let idx = 0; idx < arr.length; idx++) {
    const col_name = column_info[idx]["name"]
    var oldChild = document.getElementById(col_name)

    if (oldChild) {
      oldChild.firstChild.value = arr[idx]
    } else {
      var div = document.createElement("div")
      div.id = col_name
      div.className = "rangCont"

      var slider = document.createElement("input")
      slider.step = 0.001
      slider.type = "range"
      slider.setAttribute("max", 1.0)
      slider.setAttribute("min", 0.0)
      slider.value = arr[idx]
      slider.className = "rangSlider"
      slider.name = col_name
      // slider.id = col_name

      var label = document.createElement("label")
      label.htmlFor = col_name
      label.innerText = col_name
      label.className = "featureLabel"

      div.appendChild(slider)
      div.appendChild(label)
      el.appendChild(div)
    }
  }
}

function makeLabel(y) {
  var meter = document.createElement("meter")
  meter.max = 1
  meter.min = 0
  meter.value = y
  meter.className = "yOutput"
  var el = document.getElementById("dataLabels")
  el.appendChild(meter)
}

function fixArrForPlot(arr) {
  if (arr[0][0].length !== undefined) return arr[0]
  return arr
}

function plotActivations(activations_obj) {
  const activationsElement = document.getElementById("activations")
  const differences = activations_obj["differences"]

  Object.keys(activations_obj).forEach((layer_name) => {
    var activation_arr = fixArrForPlot(activations_obj[layer_name])

    var data, layout
    ;[data, layout] = makePlotObj(activation_arr, layer_name)

    var oldPlot = document.getElementById(layer_name)
    if (oldPlot) {
      Plotly.update(oldPlot.firstChild, data, layout)
    } else {
      var layerDiv = document.createElement("div")
      var plotDiv = document.createElement("div")
      layerDiv.id = layer_name
      layerDiv.className = "plotCont"
      layerDiv.appendChild(plotDiv)
      activationsElement.appendChild(layerDiv)
      Plotly.newPlot(plotDiv, data, layout)
    }
  })
}

function addDifferences(activations_obj, norm_obj) {
  Object.keys(activations_obj).forEach((layer_name) => {
    var layerDiv = document.getElementById(layer_name)

    var activation_arr = fixArrForPlot(activations_obj[layer_name])
    var norm = norm_obj[layer_name]

    var data, layout
    ;[data, layout] = makePlotObj(
      activation_arr,
      `${layer_name}_differences: ${norm}`
    )

    if (layerDiv.lastChild.className.includes("act_diff")) {
      // update
      Plotly.update(layerDiv.lastChild, data, layout)
    } else {
      var plotDiv = document.createElement("div")
      plotDiv.className = "act_diff"
      layerDiv.appendChild(plotDiv)
      Plotly.newPlot(plotDiv, data, layout)
    }
  })
}

function makePlotObj(activation_arr, layer_name) {
  var data = [
    {
      z: activation_arr,
      type: "heatmap",
      showscale: false,
      colorscale: "Greys",
    },
  ]

  var layout = {
    autosize: false,
    title: {
      text: `plot for ${layer_name}`,
      font: {
        family: "Courier New, monospace",
        // size: 24,
      },
      // xref: "paper",
      // x: 0.05,
    },
    showlegend: false,
    xaxis: {
      ticks: "",
      showticklabels: false,
      showgrid: false,
      zeroline: false,
    },
    // yaxis: {
    //   ticks: "",
    //   showticklabels: false,
    //   showgrid: false,
    //   zeroline: false,
    // },
  }

  return [data, layout]
}

;(async () => {
  var data = await fetchData()

  makeRangeSliders(data["sample_id"], data["x"][0], data["columns"])
  makeLabel(data["y"])
  plotActivations(data["activations"])
})()
